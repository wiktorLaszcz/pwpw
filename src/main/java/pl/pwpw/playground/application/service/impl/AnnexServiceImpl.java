package pl.pwpw.playground.application.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import pl.pwpw.playground.application.model.Annex;
import pl.pwpw.playground.application.model.Application;
import pl.pwpw.playground.application.repository.AnnexRepository;
import pl.pwpw.playground.application.repository.ApplicationRepository;
import pl.pwpw.playground.application.service.AnnexService;

import java.io.IOException;
import java.util.Base64;
import java.util.Date;
import java.util.Optional;

@Service
public class AnnexServiceImpl implements AnnexService {

    @Autowired
    private AnnexRepository annexRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Transactional
    @Override
    public Annex createNewAnnex(MultipartFile newAnnex, String appId) {

        Annex annex = convertToModel(newAnnex);

        if (!StringUtils.isEmpty(appId)) {
            Optional<Application> app = applicationRepository.findById(Long.valueOf(appId));
            if (app.isPresent()) {
                annex.setApplication(app.get());
            }
        }

        return annexRepository.save(annex);
    }

    private Annex convertToModel(MultipartFile newAnnex) {
        Annex annex = new Annex();
        try {
            annex.setBody(Base64.getEncoder().encodeToString(newAnnex.getBytes()));
            annex.setType(newAnnex.getContentType());
            annex.setPublicDate(new Date());
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }

        return annex;
    }
}
