package pl.pwpw.playground.application.service;

import pl.pwpw.playground.application.model.Application;
import pl.pwpw.playground.application.model.ContactDetails;

public interface ApplicationService {

    public ContactDetails findContactDetailsByAppNumber(String number);

    public Application findAppByEmail(String email);
}