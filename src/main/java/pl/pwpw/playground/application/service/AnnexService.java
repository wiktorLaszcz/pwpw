package pl.pwpw.playground.application.service;

import org.springframework.web.multipart.MultipartFile;
import pl.pwpw.playground.application.model.Annex;

public interface AnnexService {

    public Annex createNewAnnex(MultipartFile newAnnex, String appId);
}
