package pl.pwpw.playground.application.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import pl.pwpw.playground.application.model.Application;
import pl.pwpw.playground.application.model.ApplicationNumber;
import pl.pwpw.playground.application.model.ContactDetails;
import pl.pwpw.playground.application.model.EmailAddress;
import pl.pwpw.playground.application.repository.ApplicationRepository;
import pl.pwpw.playground.application.service.ApplicationService;

@Service
public class ApplicationServiceImpl implements ApplicationService {

    private static final String NOT_FOUND_BY_NUMBER = "Dane teleadresowe wniosku nie znalezione";
    private static final String NOT_FOUND_BY_EMAIL = "Wniosku nie znalezione";
    @Autowired
    private ApplicationRepository applicationRepository;

    @Override
    public ContactDetails findContactDetailsByAppNumber(String number) {
        ApplicationNumber appNum = new ApplicationNumber();
        appNum.setApplicationNumber(number);

        return applicationRepository.findContactByNumber(appNum).orElseThrow(() -> new ResponseStatusException(
                HttpStatus.NOT_FOUND, NOT_FOUND_BY_NUMBER
        ));
    }

    @Override
    public Application findAppByEmail(String email) {
        EmailAddress address = new EmailAddress(email);

        return applicationRepository.findAppByEmail(address).orElseThrow(() -> new ResponseStatusException(
                HttpStatus.NOT_FOUND, NOT_FOUND_BY_EMAIL
        ));
    }
}
