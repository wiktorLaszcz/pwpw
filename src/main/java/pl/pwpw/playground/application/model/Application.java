package pl.pwpw.playground.application.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pwpw.playground.application.model.utils.ApplicationType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
@Data
@NoArgsConstructor
@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "appId",
        scope = Application.class
)
public class Application implements Serializable {
    @Id
    @SequenceGenerator(name = "app_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app_id_seq")
    private Long appId;
    @Embedded
    private ApplicationNumber applicationNumber;
    private String firstName;
    private String lastName;
    @Embedded
    private ContactDetails contactDetails;

    @Enumerated(EnumType.STRING)
    private ApplicationType applicationType;

    @OneToMany(mappedBy = "application", cascade = CascadeType.ALL)
    private List<Annex> annex;

    public Application(ApplicationType applicationType, ApplicationNumber applicationNumber, String lastName) {
        this.applicationType = applicationType;
        this.applicationNumber = applicationNumber;
        this.lastName = lastName;
    }

}
