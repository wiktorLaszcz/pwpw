package pl.pwpw.playground.application.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;

/**
 *
 */
@Embeddable
@Setter
@Getter
public class ApplicationNumber {
    private String applicationNumber;
}
