package pl.pwpw.playground.application.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "annexId",
        scope = Annex.class
)
public class Annex implements Serializable {

    @Id
    @SequenceGenerator(name = "annex_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "annex_id_seq")
    private Long annexId;

    private String type;

    @Lob
    private String body;

    private Date publicDate;

    @ManyToOne
    @JoinColumn(name = "fk_appId")
    private Application application;


}
