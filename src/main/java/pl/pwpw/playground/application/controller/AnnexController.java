package pl.pwpw.playground.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.pwpw.playground.application.controller.valid.AnnexControllerValid;
import pl.pwpw.playground.application.model.Annex;
import pl.pwpw.playground.application.service.AnnexService;

import java.net.URI;

@RestController
@RequestMapping("/api/annex")
public class AnnexController {

    @Autowired
    private AnnexService annexService;

    @Autowired
    private AnnexControllerValid valid;

    @PostMapping("/upload")
    public ResponseEntity<Annex> up(@RequestParam("file") MultipartFile newAnnex, @RequestParam(name="appId", defaultValue = "") String appId) {
        valid.validAnnex(newAnnex);

        final Annex response = annexService.createNewAnnex(newAnnex, appId);
        final URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}")
                .buildAndExpand(response.getAnnexId()).toUri();

        return ResponseEntity.created(uri).body(response);
    }
}
