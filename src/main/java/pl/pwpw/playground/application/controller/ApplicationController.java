package pl.pwpw.playground.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.pwpw.playground.application.model.Application;
import pl.pwpw.playground.application.model.ContactDetails;
import pl.pwpw.playground.application.service.ApplicationService;

@RestController
@RequestMapping("/api/application")
public class ApplicationController {

    @Autowired
    private ApplicationService applicationService;

    @GetMapping("/{country}/{date}/{num}/contact_details")
    public ResponseEntity<ContactDetails> contactDetailsByAppNumber(@PathVariable final String country, @PathVariable final String date, @PathVariable final String num) {

        String appNumber = String.join("/", country, date, num);
        ContactDetails response = applicationService.findContactDetailsByAppNumber(appNumber);

        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/by_email")
    public ResponseEntity<Application> appByEmail(@RequestParam(value = "address", defaultValue = "") final String email) {

        Application response = applicationService.findAppByEmail(email);

        return ResponseEntity.ok().body(response);
    }

}
