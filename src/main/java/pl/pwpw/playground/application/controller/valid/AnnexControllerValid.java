package pl.pwpw.playground.application.controller.valid;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Component
public class AnnexControllerValid {

    public void validAnnex(MultipartFile annex){
        List<String> badValues = new ArrayList<>();

        if (!"application/jpg".equals(annex.getContentType()) && !"application/pdf".equals(annex.getContentType())){
            badValues.add("type");
        }

        if (!CollectionUtils.isEmpty(badValues)){
            throw new ResponseStatusException( HttpStatus.BAD_REQUEST, badValues.toString());
        }
    }
}
