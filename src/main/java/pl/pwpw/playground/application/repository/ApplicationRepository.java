package pl.pwpw.playground.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.pwpw.playground.application.model.Application;
import pl.pwpw.playground.application.model.ApplicationNumber;
import pl.pwpw.playground.application.model.ContactDetails;
import pl.pwpw.playground.application.model.EmailAddress;

import java.util.Optional;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long> {

    @Query("SELECT a.contactDetails FROM Application a WHERE a.applicationNumber = ?1")
    public Optional<ContactDetails> findContactByNumber(ApplicationNumber number);

    @Query("SELECT new Application(a.applicationType, a.applicationNumber, a.lastName) FROM Application a WHERE a.contactDetails.emailAddress = ?1")
    public Optional<Application> findAppByEmail(EmailAddress email);
}
