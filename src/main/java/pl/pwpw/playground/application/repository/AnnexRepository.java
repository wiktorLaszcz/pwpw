package pl.pwpw.playground.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pwpw.playground.application.model.Annex;

@Repository
public interface AnnexRepository extends JpaRepository<Annex, Long> {
}
